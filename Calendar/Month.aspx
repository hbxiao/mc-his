﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Calendar_Month, App_Web_dasphova" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            我的日历
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">我的日历</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlAdd" runat="server"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">
                
                    <asp:Calendar ID="Calendar1" runat="server" BorderWidth="1px" DayNameFormat="Shortest" ShowGridLines="true" BorderColor="#DDDDDD" CssClass="calendar" OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged">
                        <DayHeaderStyle CssClass="calendar_header" />
                        <TitleStyle CssClass="calendar_title" />
                        <DayStyle VerticalAlign="Top" CssClass="calendar_day" />
                        <SelectedDayStyle CssClass="calendar_selected" />
                        <TodayDayStyle CssClass="calendar_today" />
                        <WeekendDayStyle CssClass="calendar_weekend" />
                        <OtherMonthDayStyle CssClass="calendar_other" />
                        <NextPrevStyle CssClass="calendar_next" />
                    </asp:Calendar>

                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>

</asp:Content>


