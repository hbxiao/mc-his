﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_StepEdit, App_Web_3nnyqhi4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            步骤编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">步骤编辑</li>
          </ol>
        </section>

        <section class="content">
        
            <div id="AlertDiv" runat="server"></div>

            <div class="box box-default">
                
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                    </h3>
                </div>

                <div class="box-body">

                    <div class="form-group">
                        <label><asp:Label ID="Label3" runat="server" Text="步骤名称"></asp:Label></label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label><asp:Label ID="Label2" runat="server" Text="步骤排序"></asp:Label></label>
                        <asp:TextBox ID="txtSortID" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    
                    <div class="form-group" style="position:relative;">
                      <label><asp:Label ID="Label7" runat="server" Text="审批人"></asp:Label></label>
                      <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                      <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                      <div style="position:absolute; top:31px; right:10px;">
                          <asp:HyperLink ID="hlAdd" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                      </div>
                    </div>
                  
                    <div class="form-group">
                        <asp:CheckBox ID="cbCounterSign" runat="server"></asp:CheckBox>
                        <label><asp:Label ID="Label1" runat="server" Text="会签"></asp:Label></label>
                    </div>
                    
                </div>

                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                </div>

            </div>

        </section>

      </div>
      
</asp:Content>