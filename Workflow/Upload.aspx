﻿<%@ page language="C#" masterpagefile="~/Commons/Simple.master" autoeventwireup="true" inherits="Workflow_Upload, App_Web_3nnyqhi4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSimple" Runat="Server">

      <div class="content-wrapper-simple">

        <section class="content">

          <div class="row">

            <div class="col-xs-12">
                
                <div class="box-body">

                    <div class="form-group col-md-6">
                    <label><asp:Label ID="Label11" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                              
                    <div class="form-group col-md-6">
                    <label><asp:Label ID="Label13" runat="server" Text="说明"></asp:Label></label>
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                              
                    <div class="col-md-6">

                      <div class="form-group">
                        <div class="btn btn-default btn-file">
                          <i class="fa fa-paperclip"></i> 选择文件
                          <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);SetTitle(this);"></asp:FileUpload>
                          <span id="filepath"></span>
                        </div>
                      </div>

                    </div>

                    <script type="text/javascript">
                        function SetTitle(ct_file) {
                            if (document.getElementById('ctl00_cphSimple_txtTitle') != null) {
                                document.getElementById('ctl00_cphSimple_txtTitle').value = ct_file.value.substring(ct_file.value.lastIndexOf("\\") + 1, ct_file.value.lastIndexOf("."));
                            }
                        }
                    </script>
                              
                </div>
            
                <div class="box-footer">
                    <div class="pull-right">
                    <asp:Button ID="btnUpload" runat="server" Text="上传并保存" CssClass="btn btn-primary" onclick="btnUpload_Click"></asp:Button>
                    </div>
                </div>

                <div class="box-body">
                            
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Attachment") %>'></asp:Label>
                                    <asp:Label ID="lblFileType" runat="server" Text='<%# Bind("FileType") %>'></asp:Label>
                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("CreateUser") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="id" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("FilePath") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="标题">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="说明">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="大小">
                                <ItemTemplate>
                                    <asp:Label ID="lblFileSize" runat="server" Text='<%# Bind("FileSize") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:HyperLink ID="gvDownload" runat="server" ToolTip="下载"><span class="label label-success"><i class="fa fa-download"></i> 下载</span></asp:HyperLink>
                                    <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="150px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                </div>

            </div>

          </div>

        </section>

      </div>

</asp:Content>