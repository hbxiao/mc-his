﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Note_Edit, App_Web_qp10cplf" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Src="~/Controls/CKeditor.ascx" TagName="CKeditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlType");
            if (ddl.value == "2") {
                $("#ctl00_cphMain_txtReceiver").removeAttr("onfocus");
                document.getElementById("ctl00_cphMain_txtReceiver").value = "";
                document.getElementById("ctl00_cphMain_txtReceiverID").value = "";
                document.getElementById("ctl00_cphMain_ShareDiv").style.display = "";
            }
            else {
                document.getElementById("ctl00_cphMain_txtReceiver").value = "";
                document.getElementById("ctl00_cphMain_txtReceiverID").value = "";
                document.getElementById("ctl00_cphMain_ShareDiv").style.display = "none";
            }
        }

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtFilePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            笔记管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">笔记管理</li>
          </ol>
        </section>

        <section class="content">
            <div class="row">
        
                <MojoCube:Nav id="Nav" runat="server" />
    
                <div class="col-md-9">
                
                  <div id="AlertDiv" runat="server"></div>

                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">
                          <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                      </h3>
                    </div>
                    <div class="box-body">
                      <div class="form-group">
                         <asp:DropDownList ID="ddlFolder" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                      <div class="form-group">
                         <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                      </div>
                      <div id="ShareDiv" runat="server" class="form-group" style="position:relative; display:none">
                        <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" placeholder="分享给：" onfocus="this.blur()"></asp:TextBox>
                        <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <asp:HyperLink ID="hlShare" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-share-alt"></i> 分享</span></asp:HyperLink>
                        </div>
                      </div>
                      <div class="form-group">
                         <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder="标题："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <MojoCube:CKeditor id="txtContent" runat="server" Height="400" />
                      </div>
                      <div class="form-group" style="position:relative;">
                        <asp:TextBox ID="txtFilePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="btn btn-default btn-file">
                          <i class="fa fa-paperclip"></i> 增加附件
                          <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                          <span id="filepath"></span>
                        </div>
                        <p class="help-block">5MB以内</p>
                      </div>
                    </div>
                    <div class="box-footer">
                      <div class="pull-right">
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                        <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
        </section>

      </div>

</asp:Content>


