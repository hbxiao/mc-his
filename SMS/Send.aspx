﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="SMS_Send, App_Web_s2t1310j" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            短信发送
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">短信发送</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">

                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="账号"></asp:Label></label>
                    <asp:DropDownList ID="ddlAccount" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="模板"></asp:Label></label>
                    <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="form-control select2" AutoPostBack="True" OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group hidden">
                    <label><asp:Label ID="Label2" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group" style="position:relative;">
                    <label><asp:Label ID="Label7" runat="server" Text="接收人"></asp:Label></label>
                    <small class="tips">如13800138000|张三;13800138001|李四</small>
                    <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    <div style="position:absolute; top:31px; right:20px;">
                        <asp:HyperLink ID="hlAdd" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 选择</span></asp:HyperLink>
                    </div>
                  </div>
                  
                    <div class="col-md-12 form-group">
                        <label><asp:Label ID="Label4" runat="server" Text="短信内容"></asp:Label></label>
                        <asp:TextBox ID="txtBody" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="10"></asp:TextBox>
                    </div>
                  
              </div>
                
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="发送" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>
      
</asp:Content>