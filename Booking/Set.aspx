﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Booking_Set, App_Web_ucqnooer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            预约设置
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">预约设置</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">
              <div class="row">

                  <div class="col-md-6 form-group" style="position:relative;">
                    <label><asp:Label ID="Label7" runat="server" Text="预约人"></asp:Label></label>
                    <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                    <div style="position:absolute; top:31px; right:20px;">
                        <asp:HyperLink ID="hlAdd" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:HyperLink>
                    </div>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label3" runat="server" Text="可预约天数"></asp:Label></label>
                    <asp:TextBox ID="txtDays" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="人数限制"></asp:Label></label>
                    <asp:TextBox ID="txtLimitNum" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="可预约"></asp:Label></label>
                    <br />
                    <asp:CheckBox ID="cbUse" runat="server"></asp:CheckBox>
                  </div>
                  
              </div>
              
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>