﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Contact, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    <link href="Icon/Ali1/iconfont.css" rel="stylesheet" />
    <script src="JS/jquery.min.js"></script>
    <script src="JS/jquery.lazyload.min.js"></script>
    <script src="JS/mojocube.js?v=1"></script>
    
    <script type="text/javascript">

        function searchUser()
        {
            __doPostBack("lnbSearch", "");
        }

    </script>

</head>

<body style="padding:0px; margin:0px; background:#E6E5E5; overflow-y:auto; overflow-x:hidden">
    <form id="form1" runat="server">
        
        <asp:ScriptManager id="ScriptManager1"  runat="server"></asp:ScriptManager>
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        
               <script type="text/javascript" >
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_endRequest(function(){
                        $("img.lazy").lazyload();
                    });
                </script>
             
                <div class="news_search">
                    <div class="news_search_div">
                        <asp:TextBox ID="txtKeyword" runat="server" placeholder="搜索" CssClass="news_search_txt"></asp:TextBox>
                        <button id="btnSerach" class="news_search_btn" onclick="searchUser()"><i class="icon iconfont">&#xf00a8;</i></button>
                    </div>
                </div>

                <div style="height:10px;"></div>

                <div id="ListDiv" runat="server">
                </div>
                
                <asp:LinkButton ID="lnbSearch" runat="server" OnClick="lnbSearch_Click"></asp:LinkButton>
            
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <script type="text/javascript">
            $(function () {
                $("img.lazy").lazyload();
            });
        </script>

    </form>
</body>
</html>